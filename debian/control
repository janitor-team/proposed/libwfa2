Source: libwfa2
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               d-shlibs,
               cmake,
               pkg-config
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/libwfa2
Vcs-Git: https://salsa.debian.org/med-team/libwfa2.git
Homepage: https://github.com/smarco/WFA2-lib
Rules-Requires-Root: no

Package: libwfa2-0
Architecture: any
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: exact gap-affine algorithm (shared library)
 The wavefront alignment (WFA) algorithm is an exact gap-affine algorithm
 that takes advantage of homologous regions between the sequences to
 accelerate the alignment process. Unlike to traditional dynamic
 programming algorithms that run in quadratic time, the WFA runs in time
 O(ns+s^2), proportional to the sequence length n and the alignment score
 s, using O(s^2) memory (or O(s) using the ultralow/BiWFA mode).
 Moreover, the WFA algorithm exhibits simple computational patterns that
 the modern compilers can automatically vectorize for different
 architectures without adapting the code. To intuitively illustrate why
 the WFA algorithm is so interesting, look at the following figure. The
 left panel shows the cells computed by a classical dynamic programming
 based algorithm (like Smith-Waterman or Needleman Wunsch). In contrast,
 the right panel shows the cells computed by the WFA algorithm to obtain
 the same result (i.e., the optimal alignment).

Package: libwfa2-dev
Architecture: any
Section: libdevel
Depends: libwfa2-0 (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Description: exact gap-affine algorithm (development)
 The wavefront alignment (WFA) algorithm is an exact gap-affine algorithm
 that takes advantage of homologous regions between the sequences to
 accelerate the alignment process. Unlike to traditional dynamic
 programming algorithms that run in quadratic time, the WFA runs in time
 O(ns+s^2), proportional to the sequence length n and the alignment score
 s, using O(s^2) memory (or O(s) using the ultralow/BiWFA mode).
 Moreover, the WFA algorithm exhibits simple computational patterns that
 the modern compilers can automatically vectorize for different
 architectures without adapting the code. To intuitively illustrate why
 the WFA algorithm is so interesting, look at the following figure. The
 left panel shows the cells computed by a classical dynamic programming
 based algorithm (like Smith-Waterman or Needleman Wunsch). In contrast,
 the right panel shows the cells computed by the WFA algorithm to obtain
 the same result (i.e., the optimal alignment).
 .
 This package contains the static library and the header files.
